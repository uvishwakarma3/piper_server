# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


from xlrd import open_workbook
from django.core.mail import get_connection
from django.core.mail.message import EmailMessage


def spreadsheet_to_dictionaries(spreadsheet, sheet_index=0):
    book = open_workbook(spreadsheet)
    sheet = book.sheet_by_index(sheet_index)
    
    # read header values into the list    
    keys = [sheet.cell(0, col_index).value for col_index in range(sheet.ncols)]
    
    dictionaries = []
    for row_index in range(1, sheet.nrows):
        d = {keys[col_index]: sheet.cell(row_index, col_index).value 
             for col_index in range(sheet.ncols)}
        dictionaries.append(d)
    
    return dictionaries


def send_email(subject, body, sender, recipients, host, port, username, password, use_tls=True):
    with get_connection(
        host=host,
        port=port,
        username=username,
        password=password,
        use_tls=use_tls
    ) as connection:
        EmailMessage(subject, body, sender, recipients,
                     connection=connection).send()
