$('#reset-password-form').on('submit', function(event){
    event.preventDefault();
    $form = $(this);
    $.ajax({
        url : $form.attr('action'),
        type : 'POST',
        data : $form.serialize(),
        success: update_success,
        error : update_error
    });
    return false;
});