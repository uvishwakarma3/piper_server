var active_tags = [];
var filtered_notes = [];

$('#refresh-action').on('click', function(event){
    refresh_div('#notes');
});

$('.notes-filter').on('click', function(event){
    $('#notes-filter-dropdown li').each(function(i, li) {
        if ($(li).hasClass('active')) {
            $(li).removeClass('active');
        }
    });
    $(this).closest( 'li' ).addClass('active');
    $('#notes').data('action', $(this).data('action'));
    refresh_div('#notes');
});

$('#trash-notes').on('click', function(event){
    var note_id;
    var url;
    $('#notes-list input[type=checkbox]').each(function () {
        if (this.checked) {
            url = '/note/trash/' + $(this).attr('id').split('-')[1] + '/'
            $.ajax({
                url: url,
                type: 'POST',
                async: false,
                data: {'trash': true, csrfmiddlewaretoken: getCookie('csrftoken')},
            })
        }
    });
    refresh_div('#notes');
});

$('.tag-checkbox').change(function() {
    var tag_name = $(this).attr('id').split('-')[1]
    var index;
    if(this.checked) {
        active_tags.push(tag_name)
    } else {
        index = active_tags.indexOf(tag_name)
        if (index > -1) {
            active_tags.splice(index, 1);
        }
    }
    filtered_notes = [];
    if (active_tags.length) {
        $('#notes-list li').each(function(i, li) {
            $(li).fadeOut();
        });
        $('#notes-list li').each(function(i, li) {
            if (array_contains_array($(li).data('tags').split(','), active_tags)) {
                $(li).fadeIn();
            } else {
                filtered_notes.push($(li)['context']['id']);
            }
        });
    } else {
        $('#notes-list li').each(function(i, li) {
            $(li).fadeIn();
        });
    }
});

function array_contains_array (super_set, sub_set) {
  if (0 === sub_set.length) {
    return false;
  }
  return sub_set.every(function (value) {
    return (super_set.indexOf(value) >= 0);
  });
}

$('#search-form').on('submit', function(event){
    event.preventDefault();
    var note;
    var query = $('#search-query').val();
    $('#notes-list li').each(function(i, li) {
        if (filtered_notes.indexOf($(li)['context']['id']) < 0) {
            if (query) {
                note = $(li).find($("span.mail-partial")).text();
                if (note.indexOf(query) > -1) {
                    $(li).fadeIn();
                } else {
                    $(li).fadeOut();
                }
            } else {
                $(li).fadeIn();
            }
        }
    });
});
