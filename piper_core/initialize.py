# -*- coding: utf-8 -*-
"""
* Copyright (C) Viktor Petrov, Piper I/O EOOD - All Rights Reserved.
* This file is subject to the terms and conditions defined in
* the file 'LICENSE.md', which is part of this source code package.
* Unauthorized copying of this file via any medium is strictly prohibited.
* Proprietary and confidential.
* Written by Viktor Petrov <viktor@piperpipeline.com>, 2019
"""


import datetime
from .models import AssetType, User, Step
from .api import DbApi
from .utils import get_current_user


def initialize_demo_projects():
    db_api = DbApi()
    initialize_vfx_demo(db_api)


def initialize_vfx_demo(db_api):
    project = db_api._create_entity(entity_type='Project', name='vfx_demo',
                                   description='New example project.')
    db_api.configure(entity=project, data={'start_frame': 1001, 'fps': 24})

    for i in range(1, 3):
        user = db_api.get_entity(entity_type='User', id=i)
        user.projects.add(project)
        user.save()

    user = User.objects.get(name=get_current_user())

    project.description = 'Piper VFX demo project.'
    project.save()

    assets = []
    asset_type = AssetType.objects.get(name='chr')
    assets.append(db_api._create_entity(entity_type='Asset', name='paul', project=project,
                                       description='Example VFX character asset.',
                                       asset_type=asset_type))

    asset_type = AssetType.objects.get(name='prp')
    assets.append(db_api._create_entity(entity_type='Asset', name='turntable', project=project,
                                       description='Turntable prop for VFX lookdev.',
                                       asset_type=asset_type))

    asset_type = AssetType.objects.get(name='prp')
    assets.append(db_api._create_entity(entity_type='Asset', name='reference_stand', project=project,
                                       description='Reference stand prop for VFX lookdev.', asset_type=asset_type))

    assets.append(db_api._create_entity(entity_type='Asset', name='backdrop', project=project,
                                       description='Backdrop prop for VFX lookdev.', asset_type=asset_type))

    asset_type = AssetType.objects.get(name='env')
    assets.append(db_api._create_entity(entity_type='Asset', name='lookdev_scene', project=project,
                                       description='A VFX lookdev environment scene.', asset_type=asset_type))

    for asset in assets:
        tasks = []
        step = Step.objects.get(name='mod', level='Asset')
        tasks.append(db_api._create_entity(entity_type='Task',
                                          name='mod',
                                          project=project,
                                          asset=asset,
                                          step=step,
                                          description='Example VFX asset modeling task.',
                                          start_date=datetime.datetime.now(datetime.timezone.utc) -
                                                     datetime.timedelta(days=1),
                                          end_date=datetime.datetime.now(datetime.timezone.utc) +
                                                   datetime.timedelta(days=7)))

        step = Step.objects.get(name='ldv', level='Asset')
        tasks.append(db_api._create_entity(entity_type='Task',
                                          name='ldv',
                                          project=project,
                                          asset=asset,
                                          step=step,
                                          description='Example VFX asset lookdev task.',
                                          start_date=datetime.datetime.now(datetime.timezone.utc) -
                                                     datetime.timedelta(days=2),
                                          end_date=datetime.datetime.now(datetime.timezone.utc) +
                                                   datetime.timedelta(days=7)))

        step = Step.objects.get(name='lgt', level='Asset')
        tasks.append(db_api._create_entity(entity_type='Task',
                                          name='lgt',
                                          project=project,
                                          asset=asset,
                                          step=step,
                                          description='Example VFX asset lighting task.',
                                          start_date=datetime.datetime.now(datetime.timezone.utc) -
                                                     datetime.timedelta(days=1),
                                          end_date=datetime.datetime.now(datetime.timezone.utc) +
                                                   datetime.timedelta(days=7)))

        resources = []
        for task in tasks:
            task.artists.add(user)
            task.save()
            resources.append(db_api._create_entity(entity_type='Resource', task=task, source='maya',
                                                   artist=user, name='{}_master'.format(task.name)))

        versions = []
        for resource in resources:
            versions.append(db_api._create_entity(entity_type='Version', resource=resource, artist=user))

        for version in versions:
            db_api._create_entity(entity_type='File', version=version, artist=user, format='ma')


    sequences = []

    for seq_name in ['abc', 'jkl', 'xyz']:
        sequences.append(db_api._create_entity(entity_type='Sequence', name=seq_name,
                                              description='Example VFX sequence.', project=project))

    shots = []
    for sequence in sequences:
        for shot_name in ['0010', '0020', '0030']:
            shot = db_api._create_entity(entity_type='Shot', name=shot_name, sequence=sequence,
                                         description='Example VFX shot.')
            shots.append(shot)
            step = Step.objects.get(name='plt', level='Shot')
            task = db_api._create_entity(entity_type='Task',
                                          name='bg01',
                                          project=project,
                                          shot=shot,
                                          step=step,
                                          description='Example VFX shot background plate task.',
                                          start_date=datetime.datetime.now(datetime.timezone.utc) -
                                                     datetime.timedelta(days=1),
                                          end_date=datetime.datetime.now(datetime.timezone.utc) +
                                                   datetime.timedelta(days=7))

            resource = db_api._create_entity(entity_type='Resource',
                                             task=task,
                                             artist=user,
                                             name='{}_master'.format(task.name))

            version = db_api._create_entity(entity_type='Version', resource=resource, artist=user)

            db_api._create_entity(entity_type='File', version=version, artist=user, format='jpg', is_sequence=True)

    for shot in shots:
        tasks = []
        step = Step.objects.get(name='lay', level='Shot')
        tasks.append(db_api._create_entity(entity_type='Task',
                                          name='lay',
                                          project=project,
                                          shot=shot,
                                          step=step,
                                          description='Example shot layout task.',
                                          start_date=datetime.datetime.now(datetime.timezone.utc) -
                                                     datetime.timedelta(days=1),
                                          end_date=datetime.datetime.now(datetime.timezone.utc) +
                                                   datetime.timedelta(days=7)))

        step = Step.objects.get(name='ani', level='Shot')
        tasks.append(db_api._create_entity(entity_type='Task',
                                          name='ani',
                                          project=project,
                                          shot=shot,
                                          step=step,
                                          description='Example shot animation task.',
                                          start_date=datetime.datetime.now(datetime.timezone.utc) -
                                                     datetime.timedelta(days=1),
                                          end_date=datetime.datetime.now(datetime.timezone.utc) +
                                                   datetime.timedelta(days=7)))

        step = Step.objects.get(name='lgt', level='Shot')
        tasks.append(db_api._create_entity(entity_type='Task',
                                          name='lgt',
                                          project=project,
                                          shot=shot,
                                          step=step,
                                          description='Example shot lighting task.',
                                          start_date=datetime.datetime.now(datetime.timezone.utc) -
                                                     datetime.timedelta(days=1),
                                          end_date=datetime.datetime.now(datetime.timezone.utc) +
                                                   datetime.timedelta(days=7)))

        step = Step.objects.get(name='rot', level='Shot')
        tasks.append(db_api._create_entity(entity_type='Task',
                                          name='roto',
                                          project=project,
                                          shot=shot,
                                          step=step,
                                          description='Example roto task.',
                                          start_date=datetime.datetime.now(datetime.timezone.utc) -
                                                     datetime.timedelta(days=1),
                                          end_date=datetime.datetime.now(datetime.timezone.utc) +
                                                   datetime.timedelta(days=7)))

        step = Step.objects.get(name='pnt', level='Shot')
        tasks.append(db_api._create_entity(entity_type='Task',
                                          name='paint',
                                          project=project,
                                          shot=shot,
                                          step=step,
                                          description='Example paint task.',
                                          start_date=datetime.datetime.now(datetime.timezone.utc) -
                                                     datetime.timedelta(days=1),
                                          end_date=datetime.datetime.now(datetime.timezone.utc) +
                                                   datetime.timedelta(days=7)))

        step = Step.objects.get(name='cmp', level='Shot')
        tasks.append(db_api._create_entity(entity_type='Task',
                                           name='comp',
                                           artist=user,
                                           project=project,
                                           shot=shot,
                                           step=step,
                                           description='Example compositing task.',
                                           start_date=datetime.datetime.now(datetime.timezone.utc) -
                                                      datetime.timedelta(days=1),
                                           end_date=datetime.datetime.now(datetime.timezone.utc) +
                                                    datetime.timedelta(days=7)))

        resources = []
        for task in tasks:
            task.artists.add(user)
            task.save()
            if task.step.category == '3d':
                source = 'maya'
            else:
                source = 'nuke'
            resources.append(db_api._create_entity(entity_type='Resource', task=task, source=source,
                                                   artist=user, name='{}_master'.format(task.name)))

        versions = []
        for resource in resources:
            versions.append(db_api._create_entity(entity_type='Version', resource=resource, artist=user))
            versions.append(db_api._create_entity(entity_type='Version', resource=resource, artist=user))
            versions.append(db_api._create_entity(entity_type='Version', resource=resource, artist=user))

        for version in versions:
            if version.resource.category == '3d':
                db_api._create_entity(entity_type='File', version=version, artist=user, format='ma')
                db_api._create_entity(entity_type='File', version=version, artist=user, format='fbx')
            else:
                db_api._create_entity(entity_type='File', version=version, artist=user, format='nk')
