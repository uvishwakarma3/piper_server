Django==2.0.9
django-appconf==1.0.2
django-archive==0.1.5
django-imagekit==4.0.2
djangorestframework==3.9.0
mod-wsgi==4.6.5
pilkit==2.0
Pillow==5.3.0
psycopg2-binary==2.8.2
pytz==2018.7
six==1.11.0
timecode==1.2.0
tzlocal==1.5.1
xlrd==1.1.0
xlwt==1.3.0
