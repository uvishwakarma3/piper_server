echo Setting up Apache...
powershell -Command "Invoke-WebRequest www.piperpipeline.com/resources/win32/Apache24.zip -OutFile Apache24.zip"
powershell Expand-Archive Apache24.zip -DestinationPath . -Force
echo Apache has been set up.